
public class Generator {

	public static void main(String[] args) {
		// I created this java class to generate the contents needed for a text file for Task 3. 
		generateText();
	}
	
	public static void generateText() {
		int from = 0;
		int to = 0;
		String val = "";
		int accepted = 0;
		for(int i = 1; i < 7; i++) {
			for(int j = 0; j < 12; j++) {
				from = i;
				to = i;
				if(j > 9) {
					if(j == 10)
						val = "(";
					if(j == 11)
						val = ")";
				}
				else 
					val = ("" + j);
				if(i == 6)
					accepted = 1;
				else
					accepted = 0;
				
				switch (i) {
				  case 1: //need to find (
				    if(val.equals("(")) to = i+1;
				    break;
				  case 2: //need to find 0
					  if(val.equals("0")) to = i+1;
				    break;
				  case 3: //need to find 0
					  if(val.equals("0")) to = i+1;
				    break;
				  case 4: //need to find 7
					  if(val.equals("7")) to = i+1;
				    break;
				  case 5: //need to find )
					  if(val.equals(")")) {to = i+1; accepted = 1;}
				    break;
				  case 6: //all things are found, do nothing
				    break;
				}
				
				System.out.println(from + " " + to + " " + val + "/" + accepted);
				
			}
		}
	}

}
