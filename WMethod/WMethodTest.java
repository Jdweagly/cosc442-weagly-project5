import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class WMethodTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void test1() {
		assertEquals(false, WMethod.bondRegex("("));
	}
@Test
	void test2() {
		assertEquals(false, WMethod.bondRegex("( ("));
	}
@Test
	void test3() {
		assertEquals(false, WMethod.bondRegex("( ( ("));
	}
@Test
	void test4() {
		assertEquals(false, WMethod.bondRegex("( ( )"));
	}
@Test
	void test5() {
		assertEquals(true, WMethod.bondRegex("( ( 0 0 7 )"));
	}
@Test
	void test6() {
		assertEquals(false, WMethod.bondRegex("( ( 0 7 )"));
	}
@Test
	void test7() {
		assertEquals(false, WMethod.bondRegex("( ( 7 )"));
	}
@Test
	void test8() {
		assertEquals(false, WMethod.bondRegex("( )"));
	}
@Test
	void test9() {
		assertEquals(false, WMethod.bondRegex("( ) ("));
	}
@Test
	void test10() {
		assertEquals(false, WMethod.bondRegex("( ) )"));
	}
@Test
	void test11() {
		assertEquals(true, WMethod.bondRegex("( ) 0 0 7 )"));
	}
@Test
	void test12() {
		assertEquals(false, WMethod.bondRegex("( ) 0 7 )"));
	}
@Test
	void test13() {
		assertEquals(false, WMethod.bondRegex("( ) 7 )"));
	}
@Test
	void test14() {
		assertEquals(false, WMethod.bondRegex("( 0 ("));
	}
@Test
	void test15() {
		assertEquals(false, WMethod.bondRegex("( 0 ( ("));
	}
@Test
	void test16() {
		assertEquals(false, WMethod.bondRegex("( 0 ( )"));
	}
@Test
	void test17() {
		assertEquals(true, WMethod.bondRegex("( 0 ( 0 0 7 )"));
	}
@Test
	void test18() {
		assertEquals(true, WMethod.bondRegex("( 0 ( 0 7 )"));
	}
@Test
	void test19() {
		assertEquals(false, WMethod.bondRegex("( 0 ( 7 )"));
	}
@Test
	void test20() {
		assertEquals(false, WMethod.bondRegex("( 0 )"));
	}
@Test
	void test21() {
		assertEquals(false, WMethod.bondRegex("( 0 ) ("));
	}
@Test
	void test22() {
		assertEquals(false, WMethod.bondRegex("( 0 ) )"));
	}
@Test
	void test23() {
		assertEquals(true, WMethod.bondRegex("( 0 ) 0 0 7 )"));
	}
@Test
	void test24() {
		assertEquals(true, WMethod.bondRegex("( 0 ) 0 7 )"));
	}
@Test
	void test25() {
		assertEquals(false, WMethod.bondRegex("( 0 ) 7 )"));
	}
@Test
	void test26() {
		assertEquals(false, WMethod.bondRegex("( 0 0 ("));
	}
@Test
	void test27() {
		assertEquals(false, WMethod.bondRegex("( 0 0 ( ("));
	}
@Test
	void test28() {
		assertEquals(false, WMethod.bondRegex("( 0 0 ( )"));
	}
@Test
	void test29() {
		assertEquals(true, WMethod.bondRegex("( 0 0 ( 0 0 7 )"));
	}
@Test
	void test30() {
		assertEquals(true, WMethod.bondRegex("( 0 0 ( 0 7 )"));
	}
@Test
	void test31() {
		assertEquals(true, WMethod.bondRegex("( 0 0 ( 7 )"));
	}
@Test
	void test32() {
		assertEquals(false, WMethod.bondRegex("( 0 0 )"));
	}
@Test
	void test33() {
		assertEquals(false, WMethod.bondRegex("( 0 0 ) ("));
	}
@Test
	void test34() {
		assertEquals(false, WMethod.bondRegex("( 0 0 ) )"));
	}
@Test
	void test35() {
		assertEquals(true, WMethod.bondRegex("( 0 0 ) 0 0 7 )"));
	}
@Test
	void test36() {
		assertEquals(true, WMethod.bondRegex("( 0 0 ) 0 7 )"));
	}
@Test
	void test37() {
		assertEquals(true, WMethod.bondRegex("( 0 0 ) 7 )"));
	}
@Test
	void test38() {
		assertEquals(false, WMethod.bondRegex("( 0 0 0 ("));
	}
@Test
	void test39() {
		assertEquals(false, WMethod.bondRegex("( 0 0 0 )"));
	}
@Test
	void test40() {
		assertEquals(true, WMethod.bondRegex("( 0 0 0 0 0 7 )"));
	}
@Test
	void test41() {
		assertEquals(true, WMethod.bondRegex("( 0 0 0 0 7 )"));
	}
@Test
	void test42() {
		assertEquals(true, WMethod.bondRegex("( 0 0 0 7 )"));
	}
@Test
	void test43() {
		assertEquals(false, WMethod.bondRegex("( 0 0 1 ("));
	}
@Test
	void test44() {
		assertEquals(false, WMethod.bondRegex("( 0 0 1 )"));
	}
@Test
	void test45() {
		assertEquals(true, WMethod.bondRegex("( 0 0 1 0 0 7 )"));
	}
@Test
	void test46() {
		assertEquals(true, WMethod.bondRegex("( 0 0 1 0 7 )"));
	}
@Test
	void test47() {
		assertEquals(true, WMethod.bondRegex("( 0 0 1 7 )"));
	}
@Test
	void test48() {
		assertEquals(false, WMethod.bondRegex("( 0 0 2 ("));
	}
@Test
	void test49() {
		assertEquals(false, WMethod.bondRegex("( 0 0 2 )"));
	}
@Test
	void test50() {
		assertEquals(true, WMethod.bondRegex("( 0 0 2 0 0 7 )"));
	}
@Test
	void test51() {
		assertEquals(true, WMethod.bondRegex("( 0 0 2 0 7 )"));
	}
@Test
	void test52() {
		assertEquals(true, WMethod.bondRegex("( 0 0 2 7 )"));
	}
@Test
	void test53() {
		assertEquals(false, WMethod.bondRegex("( 0 0 3 ("));
	}
@Test
	void test54() {
		assertEquals(false, WMethod.bondRegex("( 0 0 3 )"));
	}
@Test
	void test55() {
		assertEquals(true, WMethod.bondRegex("( 0 0 3 0 0 7 )"));
	}
@Test
	void test56() {
		assertEquals(true, WMethod.bondRegex("( 0 0 3 0 7 )"));
	}
@Test
	void test57() {
		assertEquals(true, WMethod.bondRegex("( 0 0 3 7 )"));
	}
@Test
	void test58() {
		assertEquals(false, WMethod.bondRegex("( 0 0 4 ("));
	}
@Test
	void test59() {
		assertEquals(false, WMethod.bondRegex("( 0 0 4 )"));
	}
@Test
	void test60() {
		assertEquals(true, WMethod.bondRegex("( 0 0 4 0 0 7 )"));
	}
@Test
	void test61() {
		assertEquals(true, WMethod.bondRegex("( 0 0 4 0 7 )"));
	}
@Test
	void test62() {
		assertEquals(true, WMethod.bondRegex("( 0 0 4 7 )"));
	}
@Test
	void test63() {
		assertEquals(false, WMethod.bondRegex("( 0 0 5 ("));
	}
@Test
	void test64() {
		assertEquals(false, WMethod.bondRegex("( 0 0 5 )"));
	}
@Test
	void test65() {
		assertEquals(true, WMethod.bondRegex("( 0 0 5 0 0 7 )"));
	}
@Test
	void test66() {
		assertEquals(true, WMethod.bondRegex("( 0 0 5 0 7 )"));
	}
@Test
	void test67() {
		assertEquals(true, WMethod.bondRegex("( 0 0 5 7 )"));
	}
@Test
	void test68() {
		assertEquals(false, WMethod.bondRegex("( 0 0 6 ("));
	}
@Test
	void test69() {
		assertEquals(false, WMethod.bondRegex("( 0 0 6 )"));
	}
@Test
	void test70() {
		assertEquals(true, WMethod.bondRegex("( 0 0 6 0 0 7 )"));
	}
@Test
	void test71() {
		assertEquals(true, WMethod.bondRegex("( 0 0 6 0 7 )"));
	}
@Test
	void test72() {
		assertEquals(true, WMethod.bondRegex("( 0 0 6 7 )"));
	}
@Test
	void test73() {
		assertEquals(false, WMethod.bondRegex("( 0 0 7 ("));
	}
@Test
	void test74() {
		assertEquals(false, WMethod.bondRegex("( 0 0 7 ( ("));
	}
@Test
	void test75() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ( )"));
	}
@Test
	void test76() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ( 0 0 7 )"));
	}
@Test
	void test77() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ( 0 7 )"));
	}
@Test
	void test78() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ( 7 )"));
	}
@Test
	void test79() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 )"));
	}
@Test
	void test80() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) ("));
	}
@Test
	void test81() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) ( ("));
	}
@Test
	void test82() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) ( )"));
	}
@Test
	void test83() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) ( 0 0 7 )"));
	}
@Test
	void test84() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) ( 0 7 )"));
	}
@Test
	void test85() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) ( 7 )"));
	}
@Test
	void test86() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) )"));
	}
@Test
	void test87() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) ) ("));
	}
@Test
	void test88() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) ) )"));
	}
@Test
	void test89() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) ) 0 0 7 )"));
	}
@Test
	void test90() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) ) 0 7 )"));
	}
@Test
	void test91() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) ) 7 )"));
	}
@Test
	void test92() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 0 ("));
	}
@Test
	void test93() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 0 )"));
	}
@Test
	void test94() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 0 0 0 7 )"));
	}
@Test
	void test95() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 0 0 7 )"));
	}
@Test
	void test96() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 0 7 )"));
	}
@Test
	void test97() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 1 ("));
	}
@Test
	void test98() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 1 )"));
	}
@Test
	void test99() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 1 0 0 7 )"));
	}
@Test
	void test100() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 1 0 7 )"));
	}
@Test
	void test101() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 1 7 )"));
	}
@Test
	void test102() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 2 ("));
	}
@Test
	void test103() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 2 )"));
	}
@Test
	void test104() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 2 0 0 7 )"));
	}
@Test
	void test105() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 2 0 7 )"));
	}
@Test
	void test106() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 2 7 )"));
	}
@Test
	void test107() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 3 ("));
	}
@Test
	void test108() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 3 )"));
	}
@Test
	void test109() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 3 0 0 7 )"));
	}
@Test
	void test110() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 3 0 7 )"));
	}
@Test
	void test111() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 3 7 )"));
	}
@Test
	void test112() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 4 ("));
	}
@Test
	void test113() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 4 )"));
	}
@Test
	void test114() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 4 0 0 7 )"));
	}
@Test
	void test115() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 4 0 7 )"));
	}
@Test
	void test116() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 4 7 )"));
	}
@Test
	void test117() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 5 ("));
	}
@Test
	void test118() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 5 )"));
	}
@Test
	void test119() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 5 0 0 7 )"));
	}
@Test
	void test120() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 5 0 7 )"));
	}
@Test
	void test121() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 5 7 )"));
	}
@Test
	void test122() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 6 ("));
	}
@Test
	void test123() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 6 )"));
	}
@Test
	void test124() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 6 0 0 7 )"));
	}
@Test
	void test125() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 6 0 7 )"));
	}
@Test
	void test126() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 6 7 )"));
	}
@Test
	void test127() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 7 ("));
	}
@Test
	void test128() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 7 )"));
	}
@Test
	void test129() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 7 0 0 7 )"));
	}
@Test
	void test130() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 7 0 7 )"));
	}
@Test
	void test131() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 7 7 )"));
	}
@Test
	void test132() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 8 ("));
	}
@Test
	void test133() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 8 )"));
	}
@Test
	void test134() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 8 0 0 7 )"));
	}
@Test
	void test135() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 8 0 7 )"));
	}
@Test
	void test136() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 8 7 )"));
	}
@Test
	void test137() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 9 ("));
	}
@Test
	void test138() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 9 )"));
	}
@Test
	void test139() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 9 0 0 7 )"));
	}
@Test
	void test140() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 9 0 7 )"));
	}
@Test
	void test141() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 ) 9 7 )"));
	}
@Test
	void test142() {
		assertEquals(false, WMethod.bondRegex("( 0 0 7 0 ("));
	}
@Test
	void test143() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 0 )"));
	}
@Test
	void test144() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 0 0 0 7 )"));
	}
@Test
	void test145() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 0 0 7 )"));
	}
@Test
	void test146() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 0 7 )"));
	}
@Test
	void test147() {
		assertEquals(false, WMethod.bondRegex("( 0 0 7 1 ("));
	}
@Test
	void test148() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 1 )"));
	}
@Test
	void test149() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 1 0 0 7 )"));
	}
@Test
	void test150() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 1 0 7 )"));
	}
@Test
	void test151() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 1 7 )"));
	}
@Test
	void test152() {
		assertEquals(false, WMethod.bondRegex("( 0 0 7 2 ("));
	}
@Test
	void test153() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 2 )"));
	}
@Test
	void test154() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 2 0 0 7 )"));
	}
@Test
	void test155() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 2 0 7 )"));
	}
@Test
	void test156() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 2 7 )"));
	}
@Test
	void test157() {
		assertEquals(false, WMethod.bondRegex("( 0 0 7 3 ("));
	}
@Test
	void test158() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 3 )"));
	}
@Test
	void test159() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 3 0 0 7 )"));
	}
@Test
	void test160() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 3 0 7 )"));
	}
@Test
	void test161() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 3 7 )"));
	}
@Test
	void test162() {
		assertEquals(false, WMethod.bondRegex("( 0 0 7 4 ("));
	}
@Test
	void test163() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 4 )"));
	}
@Test
	void test164() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 4 0 0 7 )"));
	}
@Test
	void test165() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 4 0 7 )"));
	}
@Test
	void test166() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 4 7 )"));
	}
@Test
	void test167() {
		assertEquals(false, WMethod.bondRegex("( 0 0 7 5 ("));
	}
@Test
	void test168() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 5 )"));
	}
@Test
	void test169() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 5 0 0 7 )"));
	}
@Test
	void test170() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 5 0 7 )"));
	}
@Test
	void test171() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 5 7 )"));
	}
@Test
	void test172() {
		assertEquals(false, WMethod.bondRegex("( 0 0 7 6 ("));
	}
@Test
	void test173() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 6 )"));
	}
@Test
	void test174() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 6 0 0 7 )"));
	}
@Test
	void test175() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 6 0 7 )"));
	}
@Test
	void test176() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 6 7 )"));
	}
@Test
	void test177() {
		assertEquals(false, WMethod.bondRegex("( 0 0 7 7 ("));
	}
@Test
	void test178() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 7 )"));
	}
@Test
	void test179() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 7 0 0 7 )"));
	}
@Test
	void test180() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 7 0 7 )"));
	}
@Test
	void test181() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 7 7 )"));
	}
@Test
	void test182() {
		assertEquals(false, WMethod.bondRegex("( 0 0 7 8 ("));
	}
@Test
	void test183() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 8 )"));
	}
@Test
	void test184() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 8 0 0 7 )"));
	}
@Test
	void test185() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 8 0 7 )"));
	}
@Test
	void test186() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 8 7 )"));
	}
@Test
	void test187() {
		assertEquals(false, WMethod.bondRegex("( 0 0 7 9 ("));
	}
@Test
	void test188() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 9 )"));
	}
@Test
	void test189() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 9 0 0 7 )"));
	}
@Test
	void test190() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 9 0 7 )"));
	}
@Test
	void test191() {
		assertEquals(true, WMethod.bondRegex("( 0 0 7 9 7 )"));
	}
@Test
	void test192() {
		assertEquals(false, WMethod.bondRegex("( 0 0 8 ("));
	}
@Test
	void test193() {
		assertEquals(false, WMethod.bondRegex("( 0 0 8 )"));
	}
@Test
	void test194() {
		assertEquals(true, WMethod.bondRegex("( 0 0 8 0 0 7 )"));
	}
@Test
	void test195() {
		assertEquals(true, WMethod.bondRegex("( 0 0 8 0 7 )"));
	}
@Test
	void test196() {
		assertEquals(true, WMethod.bondRegex("( 0 0 8 7 )"));
	}
@Test
	void test197() {
		assertEquals(false, WMethod.bondRegex("( 0 0 9 ("));
	}
@Test
	void test198() {
		assertEquals(false, WMethod.bondRegex("( 0 0 9 )"));
	}
@Test
	void test199() {
		assertEquals(true, WMethod.bondRegex("( 0 0 9 0 0 7 )"));
	}
@Test
	void test200() {
		assertEquals(true, WMethod.bondRegex("( 0 0 9 0 7 )"));
	}
@Test
	void test201() {
		assertEquals(true, WMethod.bondRegex("( 0 0 9 7 )"));
	}
@Test
	void test202() {
		assertEquals(false, WMethod.bondRegex("( 0 1 ("));
	}
@Test
	void test203() {
		assertEquals(false, WMethod.bondRegex("( 0 1 )"));
	}
@Test
	void test204() {
		assertEquals(true, WMethod.bondRegex("( 0 1 0 0 7 )"));
	}
@Test
	void test205() {
		assertEquals(true, WMethod.bondRegex("( 0 1 0 7 )"));
	}
@Test
	void test206() {
		assertEquals(false, WMethod.bondRegex("( 0 1 7 )"));
	}
@Test
	void test207() {
		assertEquals(false, WMethod.bondRegex("( 0 2 ("));
	}
@Test
	void test208() {
		assertEquals(false, WMethod.bondRegex("( 0 2 )"));
	}
@Test
	void test209() {
		assertEquals(true, WMethod.bondRegex("( 0 2 0 0 7 )"));
	}
@Test
	void test210() {
		assertEquals(true, WMethod.bondRegex("( 0 2 0 7 )"));
	}
@Test
	void test211() {
		assertEquals(false, WMethod.bondRegex("( 0 2 7 )"));
	}
@Test
	void test212() {
		assertEquals(false, WMethod.bondRegex("( 0 3 ("));
	}
@Test
	void test213() {
		assertEquals(false, WMethod.bondRegex("( 0 3 )"));
	}
@Test
	void test214() {
		assertEquals(true, WMethod.bondRegex("( 0 3 0 0 7 )"));
	}
@Test
	void test215() {
		assertEquals(true, WMethod.bondRegex("( 0 3 0 7 )"));
	}
@Test
	void test216() {
		assertEquals(false, WMethod.bondRegex("( 0 3 7 )"));
	}
@Test
	void test217() {
		assertEquals(false, WMethod.bondRegex("( 0 4 ("));
	}
@Test
	void test218() {
		assertEquals(false, WMethod.bondRegex("( 0 4 )"));
	}
@Test
	void test219() {
		assertEquals(true, WMethod.bondRegex("( 0 4 0 0 7 )"));
	}
@Test
	void test220() {
		assertEquals(true, WMethod.bondRegex("( 0 4 0 7 )"));
	}
@Test
	void test221() {
		assertEquals(false, WMethod.bondRegex("( 0 4 7 )"));
	}
@Test
	void test222() {
		assertEquals(false, WMethod.bondRegex("( 0 5 ("));
	}
@Test
	void test223() {
		assertEquals(false, WMethod.bondRegex("( 0 5 )"));
	}
@Test
	void test224() {
		assertEquals(true, WMethod.bondRegex("( 0 5 0 0 7 )"));
	}
@Test
	void test225() {
		assertEquals(true, WMethod.bondRegex("( 0 5 0 7 )"));
	}
@Test
	void test226() {
		assertEquals(false, WMethod.bondRegex("( 0 5 7 )"));
	}
@Test
	void test227() {
		assertEquals(false, WMethod.bondRegex("( 0 6 ("));
	}
@Test
	void test228() {
		assertEquals(false, WMethod.bondRegex("( 0 6 )"));
	}
@Test
	void test229() {
		assertEquals(true, WMethod.bondRegex("( 0 6 0 0 7 )"));
	}
@Test
	void test230() {
		assertEquals(true, WMethod.bondRegex("( 0 6 0 7 )"));
	}
@Test
	void test231() {
		assertEquals(false, WMethod.bondRegex("( 0 6 7 )"));
	}
@Test
	void test232() {
		assertEquals(false, WMethod.bondRegex("( 0 7 ("));
	}
@Test
	void test233() {
		assertEquals(false, WMethod.bondRegex("( 0 7 )"));
	}
@Test
	void test234() {
		assertEquals(true, WMethod.bondRegex("( 0 7 0 0 7 )"));
	}
@Test
	void test235() {
		assertEquals(true, WMethod.bondRegex("( 0 7 0 7 )"));
	}
@Test
	void test236() {
		assertEquals(false, WMethod.bondRegex("( 0 7 7 )"));
	}
@Test
	void test237() {
		assertEquals(false, WMethod.bondRegex("( 0 8 ("));
	}
@Test
	void test238() {
		assertEquals(false, WMethod.bondRegex("( 0 8 )"));
	}
@Test
	void test239() {
		assertEquals(true, WMethod.bondRegex("( 0 8 0 0 7 )"));
	}
@Test
	void test240() {
		assertEquals(true, WMethod.bondRegex("( 0 8 0 7 )"));
	}
@Test
	void test241() {
		assertEquals(false, WMethod.bondRegex("( 0 8 7 )"));
	}
@Test
	void test242() {
		assertEquals(false, WMethod.bondRegex("( 0 9 ("));
	}
@Test
	void test243() {
		assertEquals(false, WMethod.bondRegex("( 0 9 )"));
	}
@Test
	void test244() {
		assertEquals(true, WMethod.bondRegex("( 0 9 0 0 7 )"));
	}
@Test
	void test245() {
		assertEquals(true, WMethod.bondRegex("( 0 9 0 7 )"));
	}
@Test
	void test246() {
		assertEquals(false, WMethod.bondRegex("( 0 9 7 )"));
	}
@Test
	void test247() {
		assertEquals(false, WMethod.bondRegex("( 1 ("));
	}
@Test
	void test248() {
		assertEquals(false, WMethod.bondRegex("( 1 )"));
	}
@Test
	void test249() {
		assertEquals(true, WMethod.bondRegex("( 1 0 0 7 )"));
	}
@Test
	void test250() {
		assertEquals(false, WMethod.bondRegex("( 1 0 7 )"));
	}
@Test
	void test251() {
		assertEquals(false, WMethod.bondRegex("( 1 7 )"));
	}
@Test
	void test252() {
		assertEquals(false, WMethod.bondRegex("( 2 ("));
	}
@Test
	void test253() {
		assertEquals(false, WMethod.bondRegex("( 2 )"));
	}
@Test
	void test254() {
		assertEquals(true, WMethod.bondRegex("( 2 0 0 7 )"));
	}
@Test
	void test255() {
		assertEquals(false, WMethod.bondRegex("( 2 0 7 )"));
	}
@Test
	void test256() {
		assertEquals(false, WMethod.bondRegex("( 2 7 )"));
	}
@Test
	void test257() {
		assertEquals(false, WMethod.bondRegex("( 3 ("));
	}
@Test
	void test258() {
		assertEquals(false, WMethod.bondRegex("( 3 )"));
	}
@Test
	void test259() {
		assertEquals(true, WMethod.bondRegex("( 3 0 0 7 )"));
	}
@Test
	void test260() {
		assertEquals(false, WMethod.bondRegex("( 3 0 7 )"));
	}
@Test
	void test261() {
		assertEquals(false, WMethod.bondRegex("( 3 7 )"));
	}
@Test
	void test262() {
		assertEquals(false, WMethod.bondRegex("( 4 ("));
	}
@Test
	void test263() {
		assertEquals(false, WMethod.bondRegex("( 4 )"));
	}
@Test
	void test264() {
		assertEquals(true, WMethod.bondRegex("( 4 0 0 7 )"));
	}
@Test
	void test265() {
		assertEquals(false, WMethod.bondRegex("( 4 0 7 )"));
	}
@Test
	void test266() {
		assertEquals(false, WMethod.bondRegex("( 4 7 )"));
	}
@Test
	void test267() {
		assertEquals(false, WMethod.bondRegex("( 5 ("));
	}
@Test
	void test268() {
		assertEquals(false, WMethod.bondRegex("( 5 )"));
	}
@Test
	void test269() {
		assertEquals(true, WMethod.bondRegex("( 5 0 0 7 )"));
	}
@Test
	void test270() {
		assertEquals(false, WMethod.bondRegex("( 5 0 7 )"));
	}
@Test
	void test271() {
		assertEquals(false, WMethod.bondRegex("( 5 7 )"));
	}
@Test
	void test272() {
		assertEquals(false, WMethod.bondRegex("( 6 ("));
	}
@Test
	void test273() {
		assertEquals(false, WMethod.bondRegex("( 6 )"));
	}
@Test
	void test274() {
		assertEquals(true, WMethod.bondRegex("( 6 0 0 7 )"));
	}
@Test
	void test275() {
		assertEquals(false, WMethod.bondRegex("( 6 0 7 )"));
	}
@Test
	void test276() {
		assertEquals(false, WMethod.bondRegex("( 6 7 )"));
	}
@Test
	void test277() {
		assertEquals(false, WMethod.bondRegex("( 7 ("));
	}
@Test
	void test278() {
		assertEquals(false, WMethod.bondRegex("( 7 )"));
	}
@Test
	void test279() {
		assertEquals(true, WMethod.bondRegex("( 7 0 0 7 )"));
	}
@Test
	void test280() {
		assertEquals(false, WMethod.bondRegex("( 7 0 7 )"));
	}
@Test
	void test281() {
		assertEquals(false, WMethod.bondRegex("( 7 7 )"));
	}
@Test
	void test282() {
		assertEquals(false, WMethod.bondRegex("( 8 ("));
	}
@Test
	void test283() {
		assertEquals(false, WMethod.bondRegex("( 8 )"));
	}
@Test
	void test284() {
		assertEquals(true, WMethod.bondRegex("( 8 0 0 7 )"));
	}
@Test
	void test285() {
		assertEquals(false, WMethod.bondRegex("( 8 0 7 )"));
	}
@Test
	void test286() {
		assertEquals(false, WMethod.bondRegex("( 8 7 )"));
	}
@Test
	void test287() {
		assertEquals(false, WMethod.bondRegex("( 9 ("));
	}
@Test
	void test288() {
		assertEquals(false, WMethod.bondRegex("( 9 )"));
	}
@Test
	void test289() {
		assertEquals(true, WMethod.bondRegex("( 9 0 0 7 )"));
	}
@Test
	void test290() {
		assertEquals(false, WMethod.bondRegex("( 9 0 7 )"));
	}
@Test
	void test291() {
		assertEquals(false, WMethod.bondRegex("( 9 7 )"));
	}
@Test
	void test292() {
		assertEquals(false, WMethod.bondRegex(")"));
	}
@Test
	void test293() {
		assertEquals(false, WMethod.bondRegex(") ("));
	}
@Test
	void test294() {
		assertEquals(false, WMethod.bondRegex(") )"));
	}
@Test
	void test295() {
		assertEquals(false, WMethod.bondRegex(") 0 0 7 )"));
	}
@Test
	void test296() {
		assertEquals(false, WMethod.bondRegex(") 0 7 )"));
	}
@Test
	void test297() {
		assertEquals(false, WMethod.bondRegex(") 7 )"));
	}
@Test
	void test298() {
		assertEquals(false, WMethod.bondRegex("0 ("));
	}
@Test
	void test299() {
		assertEquals(false, WMethod.bondRegex("0 )"));
	}
@Test
	void test300() {
		assertEquals(false, WMethod.bondRegex("0 0 0 7 )"));
	}
@Test
	void test301() {
		assertEquals(false, WMethod.bondRegex("0 0 7 )"));
	}
@Test
	void test302() {
		assertEquals(false, WMethod.bondRegex("0 7 )"));
	}
@Test
	void test303() {
		assertEquals(false, WMethod.bondRegex("1 ("));
	}
@Test
	void test304() {
		assertEquals(false, WMethod.bondRegex("1 )"));
	}
@Test
	void test305() {
		assertEquals(false, WMethod.bondRegex("1 0 0 7 )"));
	}
@Test
	void test306() {
		assertEquals(false, WMethod.bondRegex("1 0 7 )"));
	}
@Test
	void test307() {
		assertEquals(false, WMethod.bondRegex("1 7 )"));
	}
@Test
	void test308() {
		assertEquals(false, WMethod.bondRegex("2 ("));
	}
@Test
	void test309() {
		assertEquals(false, WMethod.bondRegex("2 )"));
	}
@Test
	void test310() {
		assertEquals(false, WMethod.bondRegex("2 0 0 7 )"));
	}
@Test
	void test311() {
		assertEquals(false, WMethod.bondRegex("2 0 7 )"));
	}
@Test
	void test312() {
		assertEquals(false, WMethod.bondRegex("2 7 )"));
	}
@Test
	void test313() {
		assertEquals(false, WMethod.bondRegex("3 ("));
	}
@Test
	void test314() {
		assertEquals(false, WMethod.bondRegex("3 )"));
	}
@Test
	void test315() {
		assertEquals(false, WMethod.bondRegex("3 0 0 7 )"));
	}
@Test
	void test316() {
		assertEquals(false, WMethod.bondRegex("3 0 7 )"));
	}
@Test
	void test317() {
		assertEquals(false, WMethod.bondRegex("3 7 )"));
	}
@Test
	void test318() {
		assertEquals(false, WMethod.bondRegex("4 ("));
	}
@Test
	void test319() {
		assertEquals(false, WMethod.bondRegex("4 )"));
	}
@Test
	void test320() {
		assertEquals(false, WMethod.bondRegex("4 0 0 7 )"));
	}
@Test
	void test321() {
		assertEquals(false, WMethod.bondRegex("4 0 7 )"));
	}
@Test
	void test322() {
		assertEquals(false, WMethod.bondRegex("4 7 )"));
	}
@Test
	void test323() {
		assertEquals(false, WMethod.bondRegex("5 ("));
	}
@Test
	void test324() {
		assertEquals(false, WMethod.bondRegex("5 )"));
	}
@Test
	void test325() {
		assertEquals(false, WMethod.bondRegex("5 0 0 7 )"));
	}
@Test
	void test326() {
		assertEquals(false, WMethod.bondRegex("5 0 7 )"));
	}
@Test
	void test327() {
		assertEquals(false, WMethod.bondRegex("5 7 )"));
	}
@Test
	void test328() {
		assertEquals(false, WMethod.bondRegex("6 ("));
	}
@Test
	void test329() {
		assertEquals(false, WMethod.bondRegex("6 )"));
	}
@Test
	void test330() {
		assertEquals(false, WMethod.bondRegex("6 0 0 7 )"));
	}
@Test
	void test331() {
		assertEquals(false, WMethod.bondRegex("6 0 7 )"));
	}
@Test
	void test332() {
		assertEquals(false, WMethod.bondRegex("6 7 )"));
	}
@Test
	void test333() {
		assertEquals(false, WMethod.bondRegex("7 ("));
	}
@Test
	void test334() {
		assertEquals(false, WMethod.bondRegex("7 )"));
	}
@Test
	void test335() {
		assertEquals(false, WMethod.bondRegex("7 0 0 7 )"));
	}
@Test
	void test336() {
		assertEquals(false, WMethod.bondRegex("7 0 7 )"));
	}
@Test
	void test337() {
		assertEquals(false, WMethod.bondRegex("7 7 )"));
	}
@Test
	void test338() {
		assertEquals(false, WMethod.bondRegex("8 ("));
	}
@Test
	void test339() {
		assertEquals(false, WMethod.bondRegex("8 )"));
	}
@Test
	void test340() {
		assertEquals(false, WMethod.bondRegex("8 0 0 7 )"));
	}
@Test
	void test341() {
		assertEquals(false, WMethod.bondRegex("8 0 7 )"));
	}
@Test
	void test342() {
		assertEquals(false, WMethod.bondRegex("8 7 )"));
	}
@Test
	void test343() {
		assertEquals(false, WMethod.bondRegex("9 ("));
	}
@Test
	void test344() {
		assertEquals(false, WMethod.bondRegex("9 )"));
	}
@Test
	void test345() {
		assertEquals(false, WMethod.bondRegex("9 0 0 7 )"));
	}
@Test
	void test346() {
		assertEquals(false, WMethod.bondRegex("9 0 7 )"));
	}
@Test
	void test347() {
		assertEquals(false, WMethod.bondRegex("9 7 )"));
	}

}
